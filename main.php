<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700&display=swap&subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" href="js/vendor/fullpage/fullpage.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <link rel="stylesheet" href="css/main.css">

    <style>

    </style>

</head>
<body>

<div class="page">

    <nav class="nav">
        <span class="nav__close nav_toggle"></span>
        <div class="nav__wrap">
            <ul id="myMenu">
                <li><a data-page="1" href="#firstPage">Главная</a></li>
                <li><a data-page="2"  href="#secondPage">Что такое Data Marketing</a></li>
                <li><a data-page="3"  href="#3rdPage">Преимущества</a></li>
                <li><a data-page="4"  href="#4rdPage">Применение</a></li>
                <li><a data-page="5"  href="#5rdPage">ОСТАВИТЬ ЗАЯВКУ</a></li>
            </ul>
        </div>
    </nav>

    <main class="main">

        <div class="main__wrap">

            <header class="header">
                <div class="header__wrap">
                    <a href="#5rdPage" class="header__order">ОСТАВИТЬ ЗАЯВКУ</a>
                    <a href="#firstPage" class="header__logo">
                        <img src="img/logo.svg" class="img_fluid" alt="">
                    </a>
                    <a href="#" class="header__nav nav_toggle"></a>
                </div>
            </header>

            <div id="fullpage">

                <div class="section" id="section0">
                    <section class="primary screen">
                        <div class="screen__wrap">
                            <div class="container">
                                <div class="primary__title">DATA MARKETING</div>
                                <div class="primary__text">
                                    почему внедрять<br class="hide-sm-only hide-lg"/>
                                    и использовать<br class="hide-xs-only hide-md-only"/> нужно<br class="hide-sm-only hide-lg"/>
                                    уже сейчас?
                                </div>
                                <div class="text_center">
                                    <a href="#secondPage" class="btn btn_md"><span>УЗНАТЬ ПОДРОБНЕЕ</span></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="section" id="section1">
                    <section class="about screen">
                        <div class="screen__wrap">
                            <div class="container">
                                <div class="about__title">DATA MARKETING</div>
                                <div class="about__text">Data-маркетинг - это построение и использование инсайтов на основании анализа больших данных для прогнозирования поведения пользователей и оптимизации таргетинга рекламных сообщений. Это самый эффективный способ выстраивания персональной коммуникации брендов с потребителями. Мы делаем так, чтобы огромные массивы данных работали на вас.</div>
                            </div>
                        </div>
                        <a href="#3rdPage" class="btn_next">
                            <img src="img/icon__next.svg" class="img_fluid" alt="">
                        </a>
                    </section>
                </div>

                <div class="section" id="section2">
                    <section class="advantage screen">
                        <div class="screen__wrap">
                            <div class="container">
                                <div class="advantage__title">Data Marketing - <br class="hide-sm"/>наши <br class="hide-xs-only hide-sm-only"/> преимущества <br class="hide-md"/>для бизнеса</div>
                                <div class="advantage__text swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="advantage__item">Изучайте свою аудиторию: типы и стили потребляемого контента, паттерны поведения в цифровом мире, досуг, объем доходов, круг общения, радости, боли и страхи.</div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="advantage__item">Налаживайте со своей аудиторией тесную коммуникацию и предлагайте нужные ей услуги и продукты в необходимом объеме в подходящее время.</div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="advantage__item">Забирайте внимание своей аудитории у конкурентов - и получайте новых лояльных клиентов. </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="advantage__item">Анализируйте поведение и реакции ваших клиентов и подписчиков, владейте полной информацией об эффективности ваших маркетинговых кампаний. Расходуйте эффективнее, зарабатывайте больше.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="advantage__nav">
                                    <a href="#" data-slide="0" class="advantage__nav_item advantage0 active">1</a>
                                    <a href="#" data-slide="1" class="advantage__nav_item advantage1">2</a>
                                    <a href="#" data-slide="2" class="advantage__nav_item advantage2">3</a>
                                    <a href="#" data-slide="3" class="advantage__nav_item advantage3">4</a>
                                </div>
                            </div>
                        </div>
                        <a href="#4rdPage" class="btn_next">
                            <img src="img/icon__next.svg" class="img_fluid" alt="">
                        </a>
                    </section>
                </div>

                <div class="section" id="section3">
                    <section class="use screen">
                        <div class="screen__wrap">
                            <div class="container">
                                <div class="use__title">Успешные примеры <br class="hide-lg"/>использования <br class="hide-sm-only"/>Data-маркетинг</div>
                                <div class="use__content swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="use__data">
                                                <div class="use__data_row">
                                                    <div class="use__data_chart">
                                                        <div class="use__data_scale"><span style="width: 88%;"></span></div>
                                                    </div>
                                                    <div class="use__data_text">88% компаний по миру уже используют data маркетинг;</div>
                                                </div>
                                                <div class="use__data_row">
                                                    <div class="use__data_chart">
                                                        <div class="use__data_scale"><span style="width: 53%;"></span></div>
                                                    </div>
                                                    <div class="use__data_text">53% из них утверждают, что такая практика сделала их более ориентированными на удовлетворение потребностей пользователей;</div>
                                                </div>
                                                <div class="use__data_row">
                                                    <div class="use__data_chart">
                                                        <div class="use__data_scale"><span style="width: 73%;"></span></div>
                                                    </div>
                                                    <div class="use__data_text">73% отмечают, что значительно вырос показатель вовлеченности;</div>
                                                </div>
                                                <div class="use__data_row mb_0">
                                                    <div class="use__data_chart">
                                                        <div class="use__data_scale"><span style="width: 59%;"></span></div>
                                                    </div>
                                                    <div class="use__data_text">59% маркетологов утверждают, что процесс принятия решений значительно ускорился.</div>
                                                </div>
                                                <div class="use__data_row">
                                                    <div class="use__data_chart">
                                                    </div>
                                                    <div class="use__data_text">
                                                        <a href="https://maddata.agency/whats-new/data-driven-marketing-chto-v-trende" target="_blank" class="use__data_link">Источник</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="use__text">
                                                ЗАО Тандер (торговая сеть “Магнит”) обладает огромной сетью торговых точек на территории России (20 497 по состоянию на конец сентября 2019 г.) Для развития бизнеса, повышения эффективности продаж и предложения товаров потребителям в соответствии со спросом, ритейлер использует цифровые технологии. В частности, Data-маркетинг применяется в сфере категорийного мерчендайзинга и эффективного управления цепочками поставок посредством GPS.
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="use__text">
                                                Вымпелком не только использует Data-маркетинг для проведения своих эффективных рекламных кампаний, но также предлагает его в качестве платной услуги. Данные
                                                о пользователях оператор собирает по различным каналам, включая свою абонентскую базу сотовой связи, систему определения местоположения, подключения к точкам Wi-Fi-доступа, активность подписчиков ТВ-сервисов.
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="use__text">
                                                PepsiCo - это компания, производящая упакованные потребительские товары. Она использует огромные объемы данных для эффективного управления цепочками поставок,
                                                в частности, для согласования с заказчиками и прогнозирования их потребностей
                                                в производстве и отгрузке. Таким образом, компания гарантирует, что у ритейлеров есть нужные продукты, в нужных объемах и в нужное время.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="use__nav">
                                    <a href="#" data-slide="0" class="use__nav_item use0 active">Статистика</a>
                                    <a href="#" data-slide="1" class="use__nav_item use1">ЗАО Тандер</a>
                                    <a href="#" data-slide="2" class="use__nav_item use2">Вымпелком</a>
                                    <a href="#" data-slide="3" class="use__nav_item use3">PepsiCo</a>
                                </div>
                            </div>
                        </div>
                        <a href="#5rdPage" class="btn_next">
                            <img src="img/icon__next.svg" class="img_fluid" alt="">
                        </a>
                    </section>
                </div>

                <div class="section" id="section4">
                    <section class="request screen">
                        <div class="screen__wrap">
                            <div class="container">
                                <div class="request__form">
                                    <form class="form">
                                        <div class="form_group">
                                            <input type="text" class="form_control" name="name" placeholder="Как к вам обращаться?">
                                        </div>
                                        <div class="request__row">
                                            <div class="request__col">
                                                <div class="form_group">
                                                    <input type="text" class="form_control" name="email" placeholder="e-mail">
                                                    <span class="form_error">Проверьте корректность введенных данных</span>
                                                </div>
                                            </div>
                                            <div class="request__col">
                                                <div class="form_group">
                                                    <input type="text" class="form_control" name="phone" placeholder="Номер телефона">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">Сфера вашего бизнеса и примерный объем вашей аудитории</label>
                                            <textarea class="form_control" name="message" placeholder=""></textarea>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_checkbox">
                                                <input type="checkbox" name="check" value="1" checked>
                                                <span>Хочу получить спецпредложение для малого бизнеса</span>
                                            </label>
                                        </div>
                                        <div class="request__button">
                                            <button type="submit" class="btn btn_md"><span>ОСТАВИТЬ ЗАЯВКУ</span></button>
                                        </div>
                                        <div class="request__privacy">* Нажимая на кнопку “Отправить заявку”, вы соглашаетесь с <a href="#">Политикой конфиденциальности</a> и даете свое согласие на обработку персональных данных</div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>

    </main>

</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
<script src="js/vendor/jquery.maskedinput.min.js"></script>
<script src="js/vendor/fullpage/fullpage.min.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<script src="js/main.js"></script>

<script>

    var slideIndexBis = 1;
    var sliding = false;

    var myFullpage = new fullpage('#fullpage', {

        anchors: ['firstPage', 'secondPage', '3rdPage', '4rdPage', '5rdPage'],
        sectionsColor: ['#223039', '#223039', '#223039', '#223039', '#223039'],
        menu: '#myMenu',
        navigation: true,
        touchSensitivity: 30,
        verticalCentered: true,
        navigationPosition: 'right',
        navigationTooltips: ['1 page', '2 page', '3 page', '4 page', '5 page'],
        //   onLeave: function(){
        //       $('body').removeClass('nav_open');
        //   }
    });


    // Nav

    (function() {

        $('#myMenu a').on('click touchstart', function(e){
            e.preventDefault();
            $('body').removeClass('nav_open');
            var section = parseInt($(this).attr("data-page"));
            console.log(section);
            fullpage_api .moveTo(section);
        });

    }());

</script>

</body>
</html>
