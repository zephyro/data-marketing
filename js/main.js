
$(".btn_modal").fancybox({
    'padding'    : 0
});

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


// Nav

(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $('body').toggleClass('nav_open');
    });
}());


(function() {

    var advantage = new Swiper ('.advantage__text', {
        loop: false,
    });

    advantage.on('slideChange', function () {
        var sl = '.advantage' + advantage.activeIndex;
        $('.advantage__nav').find('.advantage__nav_item').removeClass('active');
        $('.advantage__nav').find(sl).addClass('active');
    });

    $('.advantage__nav_item').on('click', function(e){
        e.preventDefault();
        var slide = parseInt($(this).attr("data-slide"));
        advantage.slideTo(slide, 600);
    });

}());



(function() {

    var use = new Swiper ('.use__content', {
        loop: false,
    });


    use.on('slideChange', function () {
        var sl = '.use' + use.activeIndex;
        $('.use__nav').find('.use__nav_item').removeClass('active');
        $('.use__nav').find(sl).addClass('active');
    });

    $('.use__nav_item').on('click', function(e){
        e.preventDefault();
        var slide = parseInt($(this).attr("data-slide"));
        use.slideTo(slide, 600);
    });

}());


// ----- Маска ----------
jQuery(function($){
    $("input[name='phone']").mask("+7(999) 999-9999");
});
